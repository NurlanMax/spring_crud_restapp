package alseco.dz.spring.crud.dzalseco.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "t_item")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @ApiModelProperty(notes = "Id of the item", name = "id", required = true, value = "12345")
    private Long id;

    @ApiModelProperty(notes = "Name of the item", name = "title", required = true, value = "Acer Predator")
    private String title;

    @ApiModelProperty(notes = "Price of the item", name = "price", required = true, value = "500245")
    private int price;

}
