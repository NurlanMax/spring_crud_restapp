package alseco.dz.spring.crud.dzalseco.rest;

import alseco.dz.spring.crud.dzalseco.dto.IdDTO;
import alseco.dz.spring.crud.dzalseco.entity.Item;
import alseco.dz.spring.crud.dzalseco.entity.Staff;
import alseco.dz.spring.crud.dzalseco.repo.ItemRepo;
import alseco.dz.spring.crud.dzalseco.repo.StaffRepo;
import alseco.dz.spring.crud.dzalseco.service.ItemService;
import alseco.dz.spring.crud.dzalseco.service.StaffService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.NamedBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;

@ApiOperation(value = "/api", tags = "Staff and items profile controller")
@RestController
@RequestMapping("/api")
public class Controller {
    private final ItemService itemService;
    private final StaffService staffService;
    public Controller(StaffService staffService, ItemService itemService) {
        this.staffService = staffService;
        this.itemService = itemService;
    }

    @ApiOperation(value="add staff")
    @PostMapping(value = "/addstaff")
    public ResponseEntity<?> addStaff(@RequestBody Staff staff) {
        staffService.addStaff(staff);
        return ResponseEntity.ok(staff);
    }

    @ApiOperation(value="Get all staff", response = Staff.class)
    @GetMapping(value = "/getallstaff")
    public ResponseEntity<?> getAllStaff() {
        List<Staff> staffs = staffService.getAllStuff();
        return new ResponseEntity<>(staffs, HttpStatus.OK);
    }

    @ApiOperation(value="Get staff by id", response = Staff.class)
    @GetMapping(value = "/getstaffbyid/{id}")
    public ResponseEntity<?> getStaffById(@PathVariable(name = "id") Long id) {
        try {
            Staff staff = staffService.getStaffById(id);
            if (staff != null) {
                return new ResponseEntity<>(staff, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No such user found by given id");
        }
        return null;
    }

    @ApiOperation(value="add item", response = Staff.class)
    @PostMapping(value = "/additem")
    public ResponseEntity<?> addItem(@RequestBody Item item) {
        itemService.addItem(item);
        return ResponseEntity.ok(item);
    }

    @ApiOperation(value="Get all items", response = Staff.class)
    @GetMapping(value = "/getitembyid/{id}")
    public ResponseEntity<?> getItemById(@PathVariable(name = "id") Long id) {
        Item item = itemService.getItemById(id);
        return new ResponseEntity<>(item, HttpStatus.OK);
    }

    @ApiOperation(value="Get all items", response = Staff.class)
    @GetMapping(value = "/getallitems")
    public ResponseEntity<?> getAllItems() {
        List<Item> items = itemService.getAllItems();
        return new ResponseEntity<>(items, HttpStatus.OK);
    }


    @ApiOperation(value="Add item to staff, requires staff and item id", response = Staff.class)
    @PostMapping(value = "/additemtostaff")
    public ResponseEntity<?> addItemToStaff(@RequestBody IdDTO idDTO) {
        Long staffId = idDTO.getStaffId();
        Long itemId = idDTO.getItemId();
        Staff staff = staffService.getStaffById(staffId);
        Item item = itemService.getItemById(itemId);
        if (staff != null && item != null) {
            staff.getItems().add(item);
            staff.setQuantity(staff.getQuantity() + 1);
            staff.setTotalCost(staff.getTotalCost() + item.getPrice());
            staffService.addStaff(staff);

            return new ResponseEntity<>(staff, HttpStatus.OK);
        }
        return null;
    }

    @ApiOperation(value="Delete item from staff", response = Staff.class)
    @PostMapping(value = "/deleteitemfromstaff")
    public ResponseEntity<?> deleteItemFromStaff(@RequestBody IdDTO idDTO) {
        Long staffId = idDTO.getStaffId();
        Long itemId = idDTO.getItemId();
        Staff staff = staffService.getStaffById(staffId);
        Item item = itemService.getItemById(itemId);
        if (staff != null && item != null) {
            staff.getItems().remove(item);
            staff.setQuantity(staff.getQuantity() - 1);
            staff.setTotalCost(staff.getTotalCost() - item.getPrice());
            staffService.addStaff(staff);
            return new ResponseEntity<>(staff, HttpStatus.OK);
        }
        return null;
    }


    @ApiOperation(value="delete staff", response = Staff.class)
    @PostMapping(value = "/deletestaff")
    public ResponseEntity<?> deleteStaff(@RequestBody Staff staff) {
        Staff checkedStaff = staffService.getStaffById(staff.getId());
        if (checkedStaff != null) {
            staffService.deleteStaff(staff);
            return ResponseEntity.ok(checkedStaff);
        }
        return ResponseEntity.ok(staff);
    }

    @ApiOperation(value="Update item by id", response = Staff.class)
    @PostMapping(value = "/updateItem/{id}")
    public ResponseEntity<?> updateItem(@PathVariable Long id, @RequestBody Item item) {
        Item foundItem = itemService.getItemById(id);
        if (foundItem != null) {
            foundItem.setTitle(item.getTitle() == null ? foundItem.getTitle() : item.getTitle());
            foundItem.setPrice(item.getPrice() == 0 ? foundItem.getPrice() : item.getPrice());
            itemService.updateItem(foundItem);
            return new ResponseEntity<>(item, HttpStatus.OK);
        }
        return null;
    }

    @ApiOperation(value="Update staff by id", response = Staff.class)
    @PostMapping(value = "/updatestaff/{id}")
    public ResponseEntity<?> updateStaff(@PathVariable Long id, @RequestBody Staff staff) {
        Staff foundStaff = staffService.getStaffById(id);
        if (foundStaff != null) {
            foundStaff.setFullName(staff.getFullName() == null ? foundStaff.getFullName() : staff.getFullName());
            foundStaff.setQuantity(staff.getQuantity() == 0 ? foundStaff.getQuantity() : staff.getQuantity());
            foundStaff.setTotalCost(staff.getTotalCost() == 0 ? foundStaff.getTotalCost() : staff.getTotalCost());
            staffService.updateStaff(foundStaff);
            return new ResponseEntity<>(foundStaff, HttpStatus.OK);
        }
        return null;
    }

    @ApiOperation(value="Search staff by name", response = Staff.class)
    @GetMapping(value = "/searchstaffbyname/{input_name}")
    public ResponseEntity<?> searchStaffByName(@PathVariable(name = "input_name") String inputName) {
        List<Staff> foundStaff = staffService.getSearchedStaff(inputName);
        return ResponseEntity.ok(foundStaff);
    }

    @ApiOperation(value="Sort staff by quantity increment", response = Staff.class)
    @GetMapping(value = "/sortbyincrementquantity")
    public ResponseEntity<?> sortByIncrementQuantity() {
        List<Staff> staffs = staffService.sortByIncrement();
        return new ResponseEntity<>(staffs, HttpStatus.OK);
    }

    @ApiOperation(value="Sort staff by quantity descending", response = Staff.class)
    @GetMapping(value = "/sortbydescrimentquantity")
    public ResponseEntity<?> sortByDescendingQuantity() {
        List<Staff> staffs = staffService.sortByDescending();
        return new ResponseEntity<>(staffs, HttpStatus.OK);
    }

    @ApiOperation(value="Sort staff by total cost ascending", response = Staff.class)
    @GetMapping(value = "/sortbyascendingtotalcost")
    public ResponseEntity<?> sortAscendingTotalCost() {
        List<Staff> staff = staffService.sortByAscendingTotalCost();
        return new ResponseEntity<>(staff, HttpStatus.OK);
    }

    @ApiOperation(value="Sort staff by total cost descending", response = Staff.class)
    @GetMapping(value = "/sortbydescendingtotalcost")
    public ResponseEntity<?> sortDescendingTotalCost() {
        List<Staff> staff = staffService.sortByDescendingTotalCost();
        return new ResponseEntity<>(staff, HttpStatus.OK);
    }

}
