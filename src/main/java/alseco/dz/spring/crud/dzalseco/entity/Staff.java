package alseco.dz.spring.crud.dzalseco.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "t_staff")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Staff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @ApiModelProperty(notes = "Id of the Staff", name = "id", required = true, value = "12345")
    private Long id;

    @ApiModelProperty(notes = "Full name of the staff", name = "fullName", required = true, value = "Ilon Mask")
    private String fullName;

    @ApiModelProperty(notes = "Quantity of items belong to staff", name = "quantity", required = true, value = "5")
    private int quantity;

    @ApiModelProperty(notes = "Total cost of all items belong to staff", name = "totalCost", required = true, value = "12345")
    @Column(name = "total_cost")
    private int totalCost;

    @ApiModelProperty(notes = "List of items belong to staff", name = "items", required = true, value = "{array}")
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Item> items;

}
