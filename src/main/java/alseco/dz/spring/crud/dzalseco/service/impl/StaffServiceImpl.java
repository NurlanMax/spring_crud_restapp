package alseco.dz.spring.crud.dzalseco.service.impl;

import alseco.dz.spring.crud.dzalseco.entity.Staff;
import alseco.dz.spring.crud.dzalseco.repo.ItemRepo;
import alseco.dz.spring.crud.dzalseco.repo.StaffRepo;
import alseco.dz.spring.crud.dzalseco.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StaffServiceImpl implements StaffService {

    private final StaffRepo staffRepo;
    public StaffServiceImpl(StaffRepo staffRepo) {
        this.staffRepo = staffRepo;
    }

    @Override
    public Staff addStaff(Staff staff) {
        return staffRepo.save(staff);
    }

    @Override
    public List<Staff> getAllStuff() {
        return staffRepo.findAll();
    }

    @Override
    public Staff getStaffById(Long id) {
        return staffRepo.findById(id).get();
    }

    @Override
    public void deleteStaff(Staff staff) {
        staffRepo.delete(staff);
    }

    @Override
    public Staff updateStaff(Staff staff) {
        return staffRepo.save(staff);
    }

    @Override
    public List<Staff> getSearchedStaff(String name) {
        return staffRepo.findAllByFullNameIgnoreCaseLike('%'+name+'%');
    }

    @Override
    public List<Staff> sortByIncrement() {
        return staffRepo.findAllByOrderByQuantityAsc();
    }

    @Override
    public List<Staff> sortByDescending() {
        return staffRepo.findAllByOrderByQuantityDesc();
    }

    @Override
    public List<Staff> sortByAscendingTotalCost() {
        return staffRepo.findAllByOrderByTotalCostAsc();
    }

    @Override
    public List<Staff> sortByDescendingTotalCost() {
        return staffRepo.findAllByOrderByTotalCostDesc();
    }

}
