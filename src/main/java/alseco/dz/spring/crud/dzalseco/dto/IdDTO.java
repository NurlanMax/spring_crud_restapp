package alseco.dz.spring.crud.dzalseco.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IdDTO {
    private Long StaffId;
    private Long ItemId;
}
