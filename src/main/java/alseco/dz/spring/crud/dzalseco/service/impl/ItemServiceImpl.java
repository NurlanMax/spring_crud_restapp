package alseco.dz.spring.crud.dzalseco.service.impl;

import alseco.dz.spring.crud.dzalseco.entity.Item;
import alseco.dz.spring.crud.dzalseco.repo.ItemRepo;
import alseco.dz.spring.crud.dzalseco.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {
   private final ItemRepo itemRepo;
    public ItemServiceImpl(ItemRepo itemRepo) {
        this.itemRepo = itemRepo;
    }

    @Override
    public Item addItem(Item item) {
        return itemRepo.save(item);
    }

    @Override
    public Item getItemById(Long id) {
        return itemRepo.findById(id).get();
    }

    @Override
    public List<Item> getAllItems() {
        return itemRepo.findAll();
    }
    @Override
    public Item updateItem(Item item) {
        return itemRepo.save(item);
    }
}
