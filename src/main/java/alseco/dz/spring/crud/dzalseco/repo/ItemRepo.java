package alseco.dz.spring.crud.dzalseco.repo;

import alseco.dz.spring.crud.dzalseco.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepo extends JpaRepository<Item, Long> {
}
