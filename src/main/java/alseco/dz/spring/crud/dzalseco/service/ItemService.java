package alseco.dz.spring.crud.dzalseco.service;

import alseco.dz.spring.crud.dzalseco.entity.Item;

import java.util.List;

public interface ItemService {
    Item addItem(Item item);
    Item getItemById(Long id);
    List<Item> getAllItems();
    Item updateItem(Item item);
}
