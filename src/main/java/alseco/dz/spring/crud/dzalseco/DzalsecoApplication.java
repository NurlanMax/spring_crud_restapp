package alseco.dz.spring.crud.dzalseco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DzalsecoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DzalsecoApplication.class, args);
    }

}
