package alseco.dz.spring.crud.dzalseco.service;

import alseco.dz.spring.crud.dzalseco.entity.Staff;

import java.util.List;

public interface StaffService {
    Staff addStaff(Staff staff);
    List<Staff> getAllStuff();
    Staff getStaffById(Long id);
    void deleteStaff(Staff staff);
    Staff updateStaff(Staff staff);
    List<Staff> getSearchedStaff(String name);

    List<Staff> sortByIncrement();
    List<Staff> sortByDescending();

    List<Staff> sortByAscendingTotalCost();
    List<Staff> sortByDescendingTotalCost();

}
