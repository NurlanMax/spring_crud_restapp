package alseco.dz.spring.crud.dzalseco.repo;

import alseco.dz.spring.crud.dzalseco.entity.Staff;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StaffRepo extends JpaRepository<Staff,Long> {
    List<Staff> findAllByFullNameIgnoreCaseLike(String name);
    List<Staff> findAllByOrderByQuantityAsc();
    List<Staff> findAllByOrderByQuantityDesc();

    List<Staff> findAllByOrderByTotalCostAsc();
    List<Staff> findAllByOrderByTotalCostDesc();
}
